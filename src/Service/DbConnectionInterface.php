<?php

namespace Antonpleteniy\Db_connector_ant\Service;

interface DbConnectionInterface
{
    public function insert($object);

    public function update($object);

    public function delete($object);

    public function select($object, $params);
}




