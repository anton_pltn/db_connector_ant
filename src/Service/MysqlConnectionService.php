<?php

namespace Antonpleteniy\Db_connector_ant\Service;

use Antonpleteniy\Db_connector_ant\Service\{DbConnectionInterface};

class MysqlConnectionService implements DbConnectionInterface
{
    private $object;
    private $data;
    private $params;
    private $id;


    public function __construct($hostname, $user, $pass, $db)
    {
        $this->connection = new \mysqli($hostname, $user, $pass, $db);
        $this->sqlHelper = new \Antonpleteniy\Db_connector_ant\Service\Helpers\SQLHelper;
    }


    public function insert($object)
    {
        $tableName = $this->sqlHelper->getTableNameFromClass($object);
        $columns = $this->sqlHelper->getColumns($object);
        $params = $this->sqlHelper->getParams($columns['columns']);

        $types = $this->sqlHelper->getTypes($columns['values']);



        ///
        $stmt = $this->connection->stmt_init();


         /// qwuery
        $sql = sprintf('INSERT INTO %s (%s) VALUES (%s)', $tableName, implode(',', $columns['columns']), $params);


        /// check  qwuery
        $stmt->prepare($sql);

        //var_dump($columns );

        /// send params 
        $stmt->bind_param($types, ...$columns['values']);



        $stmt->execute();

        return $stmt->insert_id;
    }

    public function update($object)
    {
    }

    public function delete($object)
    {
    }

    public function select($object, $params)
    {
    }
}
