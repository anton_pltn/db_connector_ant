<?php

namespace Antonpleteniy\Db_connector_ant\Service\Helpers;



class SQLHelper
{

    private $excludedProperties = [
        'id'
    ];



    private $includedMethodPrefix = [
        'is',
        'get'
    ];



    public function getTableNameFromClass($class)
    {
        /** v0.1 */
        // $arr = explode("\\", get_class($class));
        // return strtolower(array_pop($arr));


        /** v1.0 */
        // $arr = explode("\\", get_class($class));

        // return strtolower(array_pop($arr));

        //$class = get_class();

        // $reflection = new \ReflectionClass($class);
        // $comment = $reflection->getDocComment();

        //$reflection = new \ReflectionClass($class);
        $comment = $this->getReflectionClass($class)->getDocComment();


        $tag = "@TableName(";

        $string = substr($comment, strpos($comment, $tag) + strlen($tag));
        $arr = [')', '*/', "'"];
        $tableName = str_replace($arr, '', $string);

        //strtolower($tableName);
        return $tableName;
    }



    public function getColumns($class,$withExcluded = false)
    {


        /** v0.1 */
        // $arr = [];

        // foreach ($columns as $key => $value) {
        //     if (method_exists($class, sprintf('get%s', $key))) {
        //         $arr[] = $key;
        //     }
        // }

        // return $arr;


        /** v1.0 */

        $properties = $this->getReflectionClass($class)->getProperties(\ReflectionProperty::IS_PRIVATE);


       // $properties = ReflectionApi::getPrivateProperties($class);

        $result = [
            'columns' => [],
            'values' => []
        ];
        foreach ($properties as $column) {
            $columnName = $column->getName();

            if ($withExcluded && in_array($columnName, $this->excludedProperties)) {
                continue;
            }

            foreach ($this->includedMethodPrefix as $includedMethodPrefix) {
                if (method_exists($class, sprintf('%s%s', $includedMethodPrefix, ucfirst($columnName)))) {
                    $value = $class->{sprintf('%s%s', $includedMethodPrefix, ucfirst($columnName))}();

                    if ($value) {
                        $result['columns'][] = $columnName;
                        $result['values'][] = $value;
                    }
                }
            }
        }

        return $result;
    }

    public function getParams($columns)
    {

        return preg_replace('/(\w+)/', '?', implode(',', $columns));
    }

    public function getVelues($class)
    {
        $methods = $this->getReflectionClass($class)->getMethod(\ReflectionMethod::IS_PUBLIC);
    }

    public function getTypes($data)
    {
        $result = '';
        foreach ($data as $column) {
            switch (gettype($column)) {
                case 'string':
                    $result .= 's';
                    break;
                case 'integer':
                case 'boolean':
                    $result .= 'i';
                    break;
                default:
                    break;
            }
        }

        return $result;
    }

    public function getReflectionClass($class)
    {
        return new \ReflectionClass($class);
    }
}
